package io.swsb;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by swsb.
 */
@Entity
@Table(name = "db_version")
public class DbVersion
{
    @Id
    @Column(name = "db_version_id")
    private Long id;

    @NotNull
    @Max(20)
    @Column(name = "db_version")
    private Long version;

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getVersion()
    {
        return version;
    }

    public void setVersion(Long version)
    {
        this.version = version;
    }

    @Override
    public String toString()
    {
        return "DbVersion{" +
                "id=" + id +
                ", version=" + version +
                '}';
    }
}
