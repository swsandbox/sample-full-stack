package io.swsb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.AbstractModule;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.jpa.JpaPersistModule;
import com.google.inject.servlet.GuiceFilter;
import com.google.inject.servlet.ServletModule;
import com.squarespace.jersey2.guice.BootstrapUtils;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.servlet.ServletContainer;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.RollbackException;
import javax.servlet.DispatcherType;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Created by swsb.
 */
public class GuicePersistSample
{
    public static void main(String[] args) throws Exception
    {
        ServiceLocator locator = BootstrapUtils.newServiceLocator();

        List<Module> modules = new ArrayList<>();

        @Singleton
        ServletModule jerseyModule = new ServletModule()
        {
            @Override
            protected void configureServlets()
            {
                bind(HelloResource.class);
                serve("/servlet").with(new HttpServlet()
                {
                    @Override
                    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
                    {
                        resp.setContentType(MediaType.TEXT_PLAIN);
                        resp.getWriter().write("servlet ok");
                    }
                });
            }
        };
        modules.add(jerseyModule);
        modules.add(new JpaPersistModule("epic-data-pu"));
//        modules.add(new ValidationModule());
        modules.add(new AbstractModule()
        {
            @Override
            protected void configure()
            {
                bind(ObjectMapper.class).toProvider(ObjectMapperProvider.class).in(Singleton.class);
                bind(JacksonJsonProvider.class).toProvider(JacksonJsonProviderProvider.class).in(com.google.inject.Singleton.class);
            }
        });

        @SuppressWarnings("unused")
        Injector injector = BootstrapUtils.newInjector(locator, modules);
        BootstrapUtils.install(locator);

        ResourceConfig config = new ResourceConfig();
        //config.register(HelloResource.class);
        config.register(JacksonJsonProvider.class);


        ServletContainer servletContainer = new ServletContainer(config);
        ServletHolder sh = new ServletHolder(servletContainer);
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");

        FilterHolder filterHolder = new FilterHolder(GuiceFilter.class);
        context.addFilter(filterHolder, "/*", EnumSet.allOf(DispatcherType.class));
        context.addServlet(sh, "/api/*");
        server.setHandler(context);
        server.start();

        PersistService persistService = injector.getInstance(PersistService.class);
        persistService.start();


        EntityManager em = injector.getInstance(EntityManager.class);

        //retrieval
        DbVersion dbVersion = em.find(DbVersion.class, 0L);
        System.out.println(dbVersion);

        try
        {
            //persist
            dbVersion.setVersion(333L);
            em.getTransaction().begin();
            em.persist(dbVersion);
            em.getTransaction().commit();
        }
        catch (RollbackException ex)
        {
            System.out.println(((ConstraintViolationException) ex.getCause()).getConstraintViolations());
        }

//        server.stop();
//        System.exit(0);
    }
}