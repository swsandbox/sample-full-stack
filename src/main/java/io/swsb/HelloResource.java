package io.swsb;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by swsb.
 */

@Path("/hello")
public class HelloResource
{

    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @POST
    public String sayHello(@NotNull @Size(max = 20, min = 3) @QueryParam("h") String h, @Valid DbVersion dbVersion)
    {
        return h;
    }
}
