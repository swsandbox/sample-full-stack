package io.swsb;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;
import com.google.inject.Inject;

import javax.inject.Provider;

/**
 * Created by swsb.
 */
public class JacksonJsonProviderProvider implements Provider<JacksonJsonProvider>
{
    @Inject
    ObjectMapper objectMapper;

    @Override
    public JacksonJsonProvider get()
    {
        return new JacksonJsonProvider(objectMapper);
    }
}
