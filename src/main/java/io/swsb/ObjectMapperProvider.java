package io.swsb;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.inject.Provider;

/**
 * Created by swsb.
 */
public class ObjectMapperProvider implements Provider<ObjectMapper>
{
    @Override
    public ObjectMapper get()
    {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper;
    }
}
